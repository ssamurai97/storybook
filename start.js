const mongoose = require('mongoose');
//import eviroment variable
require('dotenv').config({path:'variables.env'});

//connect to mongodb



const app = require('./app');

app.set('port', process.env.PORT || 3000);

const server = app.listen(app.get('port'), () => {
    console.log(`Express is running on ➡ PORT ${server.address().port}`);
});

const express = require('express');
const routes= require('./routes/index')
const mongoose = require('mongoose');
const passport = require('passport')
require('./config/passport')(passport)

//-----------------------------------
app = express();

//app routes
app.use('/', routes);
module.exports = app;

const express = require('express')
const passport = require('passport')
const authController = require('../controllers/auth')
const router = express.Router();


router.get('/', authController.auth)
router.get('/auth/google',passport.authenticate('google',
{scope:['profile', 'email']}))

router.get('/auth/google/callback', 
  passport.authenticate('google', { failureRedirect: '/' }),
  (req, res) => {
    // Successful authentication, redirect home.
    res.redirect('/dashboard');
  });
module.exports = router; 